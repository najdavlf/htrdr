
import matplotlib.pyplot as plt
import numpy as np 
import sys 


if len(sys.argv) < 2 : print("Usage: python",sys.argv[0],"flux_map.txt"); exit(1)

fmap=sys.argv[1]

normalize=False
if len(sys.argv)==3 : print("Displaying std map"); ind=3 ; suff="_std"
elif len(sys.argv)==4 : print("Displaying normalized std map"); ind=3;normalize=True ; suff="_stdNorm"
else : print("Displaying flux map"); ind=2; suff=""

f=open(fmap,"r")
ny,nx = [int(u) for u in f.readline().split()]
fval = np.zeros((nx,ny))
for ix in range(nx) :
    for iy in range(ny) :
        lvec = f.readline().split()
        while len(lvec)<6 :
           lvec = f.readline().split()
        if normalize : norm=float(lvec[ind-1])
        else : norm=1.
        fval[-ix,-iy] = float(lvec[ind])/norm


print("shape", fval.shape  )
print("max  ", np.max(fval) )
print("min  ", np.min(fval) )
print("mean ", np.sum(fval)/nx/ny )

plt.figure()
plt.imshow(fval,origin="lower",interpolation=None,cmap="gist_heat")
plt.colorbar()
plt.savefig(fmap.split(".txt")[0]+suff+".pdf")
plt.show()
